
#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

static void* HEAP;

void test1() {
    fprintf(stdout, "\n --- Test #1. Allocating one block --- \n");

    size_t block_size = 100;

    fprintf(stdout, "\nHeap before allocating block\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\nAllocating block (size: %zu)\n", block_size);
    _malloc(sizeof(uint8_t) * block_size);

    fprintf(stdout, "\nHeap after allocating block\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\n --- Test #1 FINISHED --- \n");
}

void test2() {
    fprintf(stdout, "\n --- Test #2. Freeing one block --- \n");

    size_t block_size1 = 100;
    size_t block_size2 = 200;

    fprintf(stdout, "\nAllocating blocks (sizes: %zu, %zu)\n", block_size1, block_size2);
    void* addr = _malloc(sizeof(uint8_t) * block_size1);
    _malloc(sizeof(uint8_t) * block_size2);

    fprintf(stdout, "\nHeap before freeing one block\n");
    debug_heap(stdout, HEAP);

    _free(addr);

    fprintf(stdout, "\nHeap after freeing one block\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\n --- Test #2 FINISHED --- \n");
}

void test3() {
    fprintf(stdout, "\n --- Test #3. Freeing few blocks --- \n");

    size_t block_size1 = 250;
    size_t block_size2 = 400;
    size_t extra_block_size = 150;

    fprintf(stdout, "\nAllocating blocks (sizes: %zu, %zu) and one extra block (size: %zu)\n", block_size1, block_size2, extra_block_size);
    void* addr1 = _malloc(sizeof(uint8_t) * block_size1);
    void* addr2 = _malloc(sizeof(uint8_t) * block_size2);
    _malloc(sizeof(uint8_t) * extra_block_size);

    fprintf(stdout, "\nHeap before freeing few blocks\n");
    debug_heap(stdout, HEAP);

    _free(addr1);
    _free(addr2);

    fprintf(stdout, "\nHeap after freeing few blocks\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\n --- Test #3 FINISHED --- \n");
}

void test4() {
    fprintf(stdout, "\n --- Test #4. Mapping another region --- \n");

    size_t block_size = 10000;

    fprintf(stdout, "\nHeap before mapping another region next to last block\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\nAllocating huge block (size: %zu)\n", block_size);
    _malloc(sizeof(uint8_t) * block_size);

    fprintf(stdout, "\nHeap after mapping another region\n");
    debug_heap(stdout, HEAP);

    fprintf(stdout, "\n --- Test #4 FINISHED --- \n");
}

void test5() {
    fprintf(stdout, "\n --- Test #5. Mapping region in another place --- \n");

    const size_t block_size = 40000;

    struct block_header* tmp_block = HEAP;
    struct block_header* last = NULL;

    while (tmp_block != NULL) {
        last = tmp_block;
        tmp_block = tmp_block->next;
    }

    if (last != NULL) {
        fprintf(stdout, "\nMapping address of the next block to last");

        void* new_addr = (void*) (last->contents + last->capacity.bytes);
        new_addr = mmap(new_addr, 1000, PROT_WRITE | PROT_READ, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

        fprintf(stdout, "\nMapped address: %p.\n", new_addr);

        fprintf(stdout, "\nHeap before mapping another region\n");
        debug_heap(stderr, HEAP);

        fprintf(stdout, "\nAllocating huge block (size: %zu)\n", block_size);
        void* addr = _malloc(block_size);

        fprintf(stdout, "\nHeap after mapping another region\n");
        debug_heap(stderr, HEAP);

        fprintf(stdout, "\nFreeing huge block (size: %zu)\n", block_size);
        _free(addr);

        fprintf(stdout, "\nHeap after freeing huge block\n");
        debug_heap(stderr, HEAP);

        fprintf(stdout, "\n --- Test #5 FINISHED --- \n");
    }
}

int main() {
    HEAP = heap_init(REGION_MIN_SIZE);

    test1();
    test2();
    test3();
    test4();
    test5();

    return 0;
}
